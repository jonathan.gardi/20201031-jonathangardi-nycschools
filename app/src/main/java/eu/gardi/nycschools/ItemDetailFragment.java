package eu.gardi.nycschools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    private static final String TAG = "ItemDetailFragment";
    public static final String ARG_ITEM_DBN = "dbn";
    public static final String ARG_ITEM_SCHOOL_NAME = "school_name";
    public static final String ARG_ITEM_SAT_READ = "READ";
    public static final String ARG_ITEM_SAT_WRITE = "WRITE";
    public static final String ARG_ITEM_SAT_MATH = "MATH";
    public static final String ARG_ITEM_SAT_TAKERS = "TAKERS";
    private final static String TAG_VOLLEY_SCHOOL_DETAILED_SAT = "SSATD";
    private final static String TAG_VOLLEY_SAT_DETAILED_BY_DBN = "SATDBMD";

    public ItemDetailFragment() {
    }

    private TextView tv_dbn = null;
    private TextView detailed_SAT_read;
    private TextView detailed_SAT_write;
    private TextView detailed_SAT_math;
    private TextView school_name;
    private GradientDrawable SAT_read_circle;
    private GradientDrawable SAT_write_circle;
    private GradientDrawable SAT_math_circle;
    private TextView SAT_participants;
    private TextView address;
    private TextView phone_number;
    private TextView overview_paragraph;

    private CollapsingToolbarLayout appBarLayout;
    private RequestQueue requestQueue = null;

    private String m_dbn = null;
    private DetailedSchoolData details=null;
    private static Map<String, DetailedSchoolData> map_DB2Details = new HashMap<String, DetailedSchoolData>();

    private static class DetailedSchoolData {
        boolean bSAT_retrieved=false;
        int SAT_read = -1;
        int SAT_write = -1;
        int SAT_math = -1;
        int SAT_takers = -1;
        int total_students = -1;

        boolean bOverview_retrieved=false;
        String str_school_name = null;
        String location = null;
        String address = null;
        String phone_number = null;
        String overview_paragraph = null;
    }


    private static synchronized DetailedSchoolData getOrCreateDetails(String dbn) {
        DetailedSchoolData details = map_DB2Details.get(dbn);
        if (null == details) {
            details = new DetailedSchoolData();
            map_DB2Details.put(dbn, details);
        }
        return details;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        if ((null!=bundle) && bundle.containsKey(ARG_ITEM_DBN)) {
            m_dbn = getArguments().getString(ARG_ITEM_DBN);
            details = getOrCreateDetails(m_dbn);
            details.str_school_name = getArguments().getString(ARG_ITEM_SCHOOL_NAME);

            details.SAT_read = getArguments().getInt(ARG_ITEM_SAT_READ);
            details.SAT_write = getArguments().getInt(ARG_ITEM_SAT_WRITE);
            details.SAT_math = getArguments().getInt(ARG_ITEM_SAT_MATH);
            details.SAT_takers = getArguments().getInt(ARG_ITEM_SAT_TAKERS);

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (requestQueue != null) {
            requestQueue.cancelAll(TAG_VOLLEY_SCHOOL_DETAILED_SAT);
            requestQueue.cancelAll(TAG_VOLLEY_SAT_DETAILED_BY_DBN);
        }
    }

    private void setUISATValue(TextView textview, GradientDrawable circle, int value, String def_value) {
        if (value < 0) {
            textview.setBackground(ItemListActivity.m_stroke);
            textview.setTextColor(Color.BLACK);
        } else {
            textview.setBackground(circle);
            circle.setColor(ItemListActivity.getUIColorBasedOnSAT(value));
        }
        textview.setText((value > -1) ? (def_value + ": " + value) : def_value);
    }

    private void updateUI_SAT_participants_UI() {
        StringBuffer str = new StringBuffer(String.valueOf(details.SAT_takers) + "/" + String.valueOf(details.total_students) + " (SAT takers/total students)");
        if (details.SAT_takers > 0 && details.total_students > 0) {
            str.append(" %");
            str.append(Math.round(100.0 * details.SAT_takers / details.total_students));
        }
        SAT_participants.setText(str.toString());
    }


    @Override
    public void onResume() {
        super.onResume();
        final Context context = this.getActivity();
        if (null != m_dbn) {
            details = getOrCreateDetails(m_dbn);
            if (!details.bOverview_retrieved) {
                StringRequest stringRequest = new StringRequest(Request.Method.GET,
                        ItemListActivity.mFirebaseRemoteConfig.getString("URL_NYC_GOV_PREFIX_SCHOOLS_DATA") + "?dbn=" + m_dbn + "&" +
                                "$select=primary_address_line_1,city,zip,state_code,phone_number,overview_paragraph,total_students,location",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONArray array = new JSONArray(response);
                                    if (array.length() > 0) {
                                        JSONObject object = array.getJSONObject(0);
                                        details.address = object.getString("primary_address_line_1") + ", " +
                                                object.getString("city") + ", " +
                                                object.getString("zip") + ", " +
                                                object.getString("state_code");
                                        details.phone_number = object.getString("phone_number");
                                        details.overview_paragraph = object.getString("overview_paragraph");

                                        try {
                                            details.total_students = Integer.parseInt(object.getString("total_students"));
                                        } catch (NumberFormatException e) {
                                            Log.e(TAG, "response=" + response, e);
                                            e.printStackTrace();
                                            //Invalid integer in total_students
                                        }
                                        details.location = object.getString("location");
                                        details.bOverview_retrieved=true;
                                        updateNonSAT_UI();
                                        updateUI_SAT_participants_UI();
                                    }
                                } catch (JSONException e) {
                                    Log.e(TAG, "response=" + response, e);
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG);
                            }
                        });

                requestQueue = Volley.newRequestQueue(context);
                stringRequest.setTag(TAG_VOLLEY_SCHOOL_DETAILED_SAT);
                requestQueue.add(stringRequest);


                if ((!details.bSAT_retrieved) && (details.SAT_read < 0)) {//get SAT data only if not passed from previous activity
                    StringRequest stringRequestSAT = new StringRequest(Request.Method.GET,
                            ItemListActivity.mFirebaseRemoteConfig.getString("URL_NYC_GOV_PREFIX_SCHOOLS_SAT_DATA") + "?dbn=" + m_dbn,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONArray array = new JSONArray(response);
                                        if (array.length() > 0) {
                                            JSONObject object = array.getJSONObject(0);
                                            details.SAT_read = Integer.parseInt(object.getString("sat_critical_reading_avg_score"));
                                            details.SAT_math = Integer.parseInt(object.getString("sat_math_avg_score"));
                                            details.SAT_write = Integer.parseInt(object.getString("sat_writing_avg_score"));
                                            details.SAT_takers = Integer.parseInt(object.getString("num_of_sat_test_takers"));
                                        }
                                    } catch (JSONException e) {
                                        Log.e(TAG, "response=" + response, e);
                                        e.printStackTrace();
                                    } catch (NumberFormatException e) {
                                        Log.e(TAG, "response=" + response, e);
                                        //Invalid integers in sat_critical_reading_avg_score/sat_math_avg_score/sat_writing_avg_score
//                                    e.printStackTrace();
                                    }
                                    details.bSAT_retrieved=true;
                                    updateSAT_UI();
                                    updateUI_SAT_participants_UI();
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG);
                                }
                            });
//                stringRequest.setTag(TAG_VOLLEY_SAT_BY_DBN+":"+scoolData.dbn);
                    stringRequestSAT.setTag(TAG_VOLLEY_SAT_DETAILED_BY_DBN);
                    requestQueue.add(stringRequestSAT);
                } else {
                    updateSAT_UI();
                }

            } else {
                updateNonSAT_UI();
                updateSAT_UI();
                updateUI_SAT_participants_UI();

            }
        }
    }

    private void updateNonSAT_UI()
    {
        tv_dbn.setText(m_dbn); //This is not a recycled view (the fragment is not recycled) so we do not need to worry that the response is for another dbn
        address.setText(details.address);
        phone_number.setText(details.phone_number);
        overview_paragraph.setText(details.overview_paragraph);
    }
    private void updateSAT_UI()
    {
        setUISATValue(detailed_SAT_read, SAT_read_circle, details.SAT_read, "Read");
        setUISATValue(detailed_SAT_write, SAT_write_circle, details.SAT_write, "Write");
        setUISATValue(detailed_SAT_math, SAT_math_circle, details.SAT_math, "Math");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);
        tv_dbn=rootView.findViewById(R.id.dbn);
        detailed_SAT_read=rootView.findViewById(R.id.detailed_SAT_read);
        detailed_SAT_write=rootView.findViewById(R.id.detailed_SAT_write);
        detailed_SAT_math=rootView.findViewById(R.id.detailed_SAT_math);
        SAT_participants=rootView.findViewById(R.id.SAT_participants);
        address=rootView.findViewById(R.id.address);
        phone_number=rootView.findViewById(R.id.phone_number);
        overview_paragraph=rootView.findViewById(R.id.overview_paragraph);
        school_name=rootView.findViewById(R.id.school_name);

        SAT_read_circle=(GradientDrawable)detailed_SAT_read.getBackground();
        SAT_write_circle=(GradientDrawable)detailed_SAT_write.getBackground();
        SAT_math_circle=(GradientDrawable)detailed_SAT_math.getBackground();


        final Activity activity = this.getActivity();
        appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        DetailedSchoolData details = getOrCreateDetails(m_dbn);
        if (null!=details.str_school_name)
        {
            school_name.setText(details.str_school_name);
            if (null != appBarLayout) {
                appBarLayout.setTitle(details.str_school_name);
            }
        }
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String google_map_uri=null;
                DetailedSchoolData details = getOrCreateDetails(m_dbn);
                //TODO take latitude,longitude from SchoolData and not from location string
                if ((null!=details.location) && (!details.location.isEmpty()))
                {
                    int index=details.location.lastIndexOf('(');
                    if (index>-1 && details.location.endsWith(")"))
                    {
                        //"geo:0,0?q=1600 Amphitheatre Parkway, Mountain+View, California"
                        google_map_uri="geo:"+details.location.substring(index+1,details.location.length()-1).replace(" ","")+
                                "?q="+details.location.substring(0,index-1).trim();
                    }
                }
                if (null==google_map_uri) {
                    google_map_uri = "geo:0,0?q=" + address.getText().toString();
                }

                Uri gmmIntentUri = Uri.parse(google_map_uri);//10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(activity.getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
                else
                {
                    Snackbar.make(view, "Google Maps is not available", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
        FloatingActionButton fab_call = (FloatingActionButton) rootView.findViewById(R.id.fab_call);
        fab_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phno="tel:"+phone_number.getText().toString();

                Intent i=new Intent(Intent.ACTION_DIAL,Uri.parse(phno));
                startActivity(i);
            }
        });

        return rootView;
    }
}
