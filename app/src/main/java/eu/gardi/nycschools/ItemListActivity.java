package eu.gardi.nycschools;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
//import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.SortedList;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * JPMC - Mobile Software Engineering Opportunity - 210006014 - Android Developer - NY
 * 20201031-JonathanGardi-NYCSchools
 *
 * Few things I would have liked to improve but did not have time (had a full time job during the Thursday/Friday 48 hours of assignment):
 * 1) Make the RecyclerView part of a Fragment instead of Activity
 * 2) Use RecyclerView with CursorLoader and paginate the school information (instead of retrieving and analysing a huge JSON)
 * 3) Make a much nicer UI
 * 4) Better error handeling, especially when the JSON values are wrong (I noticed 's' value instead of SAP integer, and sometimes a missing 'council_district' JSON key)
 * 5) refactor class member variables names, make sure private/public is correct
 * 6) find out why onRestoreInstanceState is not called or onCreate with non null Bundle when returning to the activity after the details fregmant is called (might be the correct behaviour, in any case mListState is now static)
 * 7) Use strings.xml for strings
 * 8) \nycschools\ItemListActivity.java uses or overrides a deprecated API.  * Note: Recompile with -Xlint:deprecation for details.
 * 9) show on map with all schools around me
 *
 *
 *
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ItemListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = "ItemListActivity";
    public static String BUNDLE_RECYCLERVIEW_STATE = "BRVS";
    private boolean mTwoPane;
    private static List<SchoolData> mSchools = null;
    private static boolean mSchoolsInitialised = false;
    private static boolean mRetreivingSchoolsData=false;
    private RecyclerView mRecyclerView = null;
    private RequestQueue mRequestQueue = null;
    private TextView mEmptyView = null;
    private final static String TAG_VOLLEY_SCHOOL_LIST = "SL";
    private final static String TAG_VOLLEY_SAT_BY_DBN = "SATDBN";
    private static int[] colors;
    public static GradientDrawable m_stroke;
    public static int color_whitesmoke;
    private FloatingActionButton mFAB;
    private static Parcelable mListState = null; //TODO: have to figure out why onRestoreInstanceState is not called (to remove the static)
    private SearchView searchView;
    private boolean bSearchViewVisible = false;
    private static String m_filter = "";
    private ProgressBar m_progressbar = null;
    private SchoolsRecyclerViewAdapter mAdapter = null;
    public static FirebaseRemoteConfig mFirebaseRemoteConfig; //the instance is from a singleton factory
    private static FusedLocationProviderClient mFusedLocationClient; // the instance is from a singleton factory
    private static final int REQUEST_LOCATION = 2;
    private static Location mMyLocation=null;
    private final static int color_darkgreen=Color.parseColor("#00994C");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        mFAB = findViewById(R.id.fab);
        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                                mAdapter.notifyItemRangeChanged(0,mAdapter.getItemCount(),new Location("adsd"));
                if (null != searchView) {
                    bSearchViewVisible = !bSearchViewVisible;
//                    searchView.setVisibility(bSearchViewVisible?View.VISIBLE:View.GONE);

                    searchView.animate()
                            .alpha(bSearchViewVisible ? 0f : 1f)
//                            .setDuration(500)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    searchView.setVisibility(bSearchViewVisible ? View.VISIBLE : View.GONE);
                                }
                            });
                }
            }
        });
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
        mFirebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(this, new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                        if (task.isSuccessful()) {
                            boolean new_values_received= task.getResult();
                            Log.d(TAG, "RemoteConfig[Success. new_values_received="+new_values_received+",version="+mFirebaseRemoteConfig.getString("version")+"]");

                        } else {
                            Log.e(TAG,"Failed to fetch FirebaseRemoteConfig");
                        }
                    }
                });

        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        mRecyclerView = findViewById(R.id.item_list);
        mEmptyView = findViewById(R.id.empty_view);
        m_progressbar = findViewById(R.id.progressbar);
        m_progressbar.setVisibility(View.GONE);
        TypedArray ta = this.getResources().obtainTypedArray(R.array.color_level);
        colors = new int[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            colors[i] = ta.getColor(i, 0);
        }
        ta.recycle();
        m_stroke = (GradientDrawable) this.getResources().getDrawable(R.drawable.stroke_shape);
        m_stroke.setColor(Color.WHITE);
        color_whitesmoke = this.getResources().getColor(R.color.whitesmoke);
//            m_circle = (GradientDrawable) mParentActivity.getResources().getDrawable(R.drawable.circle_shape);
        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(BUNDLE_RECYCLERVIEW_STATE);
        }
        searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);
        if (null == mSchools) {
            mSchools = new ArrayList<SchoolData>();
        }
        mRequestQueue = Volley.newRequestQueue(this);
//        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) ||
            (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED))
        {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION);
        }
        else
        {
            getCurrentGEOLocation(false);
        }


    }
    private void getCurrentGEOLocation(final boolean bFirstLocationAfterPermissionGranted)
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
        {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            boolean bFirstLocation=(null==mMyLocation);
                            mMyLocation=location;
                            if (bFirstLocation || bFirstLocationAfterPermissionGranted)
                            {
                                RecyclerView.Adapter adapter=mRecyclerView.getAdapter();
                                //Make sure we did not get here before onResume (which is before mRecyclerView has an adapter)
                                if (null!=adapter) {
/*
                                    //force bindDistance on all visible elements, so 'distance' will be re-calculated with the new location
                                    //Find only the positions of visible items using the mRecyclerView.getLayoutManager()
                                    LinearLayoutManager llm=(LinearLayoutManager)mRecyclerView.getLayoutManager();
                                    int firstVisiblePosition=llm.findFirstVisibleItemPosition();
                                    int lastVisiblePosition=llm.findLastVisibleItemPosition();
                                    if (RecyclerView.NO_POSITION!=firstVisiblePosition &&
                                        RecyclerView.NO_POSITION!=lastVisiblePosition) {
                                        mAdapter.notifyItemRangeChanged(firstVisiblePosition,lastVisiblePosition-firstVisiblePosition+1, location);
                                    }*/

                                    //On the first time we get a location, we need to calculate distance and re-sort everything based on distance
                                    //TODO: make this work with notifyItemRangeChanged above
                                    refreshDisplay(); //This will update distance text and re-sort by distance
//                                    mRecyclerView.setAdapter(null);
//                                    mRecyclerView.setAdapter(adapter);
//                                    adapter.notifyDataSetChanged(); //This will only update distance text, but will not re-sort
                                }
                            }
//                                mMyLocation.setLatitude(40.730839);
//                                mMyLocation.setLongitude(-73.976840);
//                                Toast.makeText(getApplicationContext(),location.getString(), Toast.LENGTH_LONG);
                        }
                    }
                });

        }
    }
    public static int getUIColorBasedOnSAT(int SATvalue) {
        int color_level = (colors.length * (SATvalue - 200) / 600); //SAT values 200-600
        if (color_level < 0) color_level = 0;
        if (color_level >= colors.length) color_level = colors.length - 1;
        return colors[color_level];
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        if (REQUEST_LOCATION==requestCode) {
            if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getCurrentGEOLocation(true);
            }
        }
    }
    private static final Comparator<SchoolData> ALPHABETICAL_OR_DISTANCE_COMPARATOR = new Comparator<SchoolData>() {
        @Override
        public int compare(SchoolData a, SchoolData b) {
            if (null==mMyLocation) {
                return a.school_name.compareTo(b.school_name);
            }
            else
            {
                float distance=mMyLocation.distanceTo(a.location)-mMyLocation.distanceTo(b.location); //in meters
                return Math.round(distance);
            }
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        // Save list state
        mListState = mRecyclerView.getLayoutManager().onSaveInstanceState();
        state.putParcelable(BUNDLE_RECYCLERVIEW_STATE,mListState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);

        // Retrieve list state and list/item positions
        if(state != null) {
            mListState = state.getParcelable(BUNDLE_RECYCLERVIEW_STATE);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if (null==mAdapter) {
            mAdapter = new SchoolsRecyclerViewAdapter(this, mTwoPane, ALPHABETICAL_OR_DISTANCE_COMPARATOR);
            mRecyclerView.setAdapter(mAdapter);
        }
        setupRecyclerView();
        if (mListState != null) {
            mRecyclerView.getLayoutManager().onRestoreInstanceState(mListState);
        }
        if (!m_filter.trim().isEmpty())
        {
            searchView.setQuery(m_filter,false);
        }
    }
    @Override
    protected void onStop () {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG_VOLLEY_SAT_BY_DBN);
            mRequestQueue.cancelAll(TAG_VOLLEY_SCHOOL_LIST);
        }
    }

    @Override
    public void onCreateSupportNavigateUpTaskStack(@NonNull TaskStackBuilder builder) {
        super.onCreateSupportNavigateUpTaskStack(builder);
    }

    private void refreshDisplay()
    {
        if ((null==mRecyclerView) || (null==mRecyclerView.getAdapter()))
        {
            Log.e(TAG,"mRecyclerView="+mRecyclerView+",getAdapter="+mRecyclerView.getAdapter());
            return;
        }

        if (mSchools.isEmpty())
        {
            mRecyclerView.setVisibility(View.GONE);
//                                    mFAB.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
            if (!mRetreivingSchoolsData && mSchoolsInitialised) {
                mEmptyView.setText("NYC school date is not available.");
            }
        }
        else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
            onQueryTextChange(m_filter);
        }
    }
    private void setupRecyclerView() {
        if (!mSchoolsInitialised) {
            mSchoolsInitialised=true; //Do once. Server's school list is static
            mRetreivingSchoolsData=true;
//            final ProgressDialog progressDialog = new ProgressDialog(this);//deprecated, replace withe modern progress bar
//            progressDialog.setMessage("Retrieving data..");
//            progressDialog.show();

            String where="";
/*
        //Filtering is now done on the existing values using a SortedList and a location distance comparator
            if (!m_filter.trim().isEmpty()) {;
                String str=m_filter.trim().toLowerCase();
                where = "&$where=" +
                        Uri.encode("lower(school_name) like '%"+str+"%'"+
                                " or lower(dbn) like '%"+str+"%'"+
                                " or lower(primary_address_line_1) like '%"+str+"%'"+
                                " or lower(city) like '%"+str+"%'"+
                                " or lower(zip) like '%"+str+"%'"
                        );
            }

 */
            String url=
//                    "https://data.cityofnewyork.us/resource/f9bf-2cp4.json",
//                    "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?dbn=21K728",
                    ItemListActivity.mFirebaseRemoteConfig.getString("URL_NYC_GOV_PREFIX_SCHOOLS_DATA")+"?"+
                            "$select=dbn,school_name,primary_address_line_1"//+,city,zip
                            +",latitude,longitude&$order=school_name%20ASC"+where;
            StringRequest stringRequest = new StringRequest(Request.Method.GET,url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            progressDialog.dismiss();
                        try {
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++) {
//                                    final String str="Parsing NYC schools data "+String.format("%d/%d",i+1,array.length());
                                JSONObject object = array.getJSONObject(i);
                                double latitude=0;
                                if (object.has("latitude")) {
                                    latitude=object.getDouble("latitude");
                                }
                                double longitude=0;
                                if (object.has("longitude")) {
                                    longitude=object.getDouble("longitude");
                                }
                                mSchools.add(new SchoolData(i,
                                        object.getString("dbn"),
                                        object.getString("school_name"),
                                        object.getString("primary_address_line_1"),//+", "+object.getString("city")+", "+object.getString("zip"),
                                        latitude,
                                        longitude
                                ));
                            }
                            mRetreivingSchoolsData=false;
                            m_progressbar.setVisibility(View.GONE);
                            mEmptyView.setVisibility(View.GONE);
                            refreshDisplay();
                        } catch (JSONException e) {
                            Log.e(TAG,"response="+response,e);
                            e.printStackTrace();
                        }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        mRetreivingSchoolsData=false;
                        m_progressbar.setVisibility(View.GONE);
//                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG);
                        mRecyclerView.setVisibility(View.GONE);
                        mEmptyView.setVisibility(View.VISIBLE);
                        mEmptyView.setText("ERROR: "+error.getMessage());
                        }
                    });
            m_progressbar.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.VISIBLE);
            mEmptyView.setText("Retrieving NYC schools data..");
            stringRequest.setTag(TAG_VOLLEY_SCHOOL_LIST);
            mRequestQueue.add(stringRequest);
        }
        else //mSchools already populated
        {
            refreshDisplay();
        }
    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }
    private static List<SchoolData> filter(List<SchoolData> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<SchoolData> filteredModelList = new ArrayList<>();
        for (SchoolData model : models) {
            if (model.school_name.toLowerCase().contains(lowerCaseQuery) ||
                model.address.toLowerCase().contains(lowerCaseQuery) ||
                model.dbn.toLowerCase().contains(lowerCaseQuery))
            {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
    @Override
    public boolean onQueryTextChange(String newText) {
        m_filter=newText;
        if (m_filter.trim().isEmpty())
        {
            mAdapter.replaceAll(mSchools);
        }
        else {
            List<SchoolData> filteredModelList = filter(mSchools, newText);
            mAdapter.replaceAll(filteredModelList);
        }
        mRecyclerView.scrollToPosition(0);
        return true;
    }

    public static class SchoolsRecyclerViewAdapter
            extends RecyclerView.Adapter<SchoolsRecyclerViewAdapter.SchoolViewHolder> {
        private final ItemListActivity mParentActivity;
        private final boolean mTwoPane;

        //We use SortedList to show schools based on distance from current GEO location
        private final SortedList<SchoolData> mSortedList = new SortedList<>(SchoolData.class,
                new SortedList.Callback<SchoolData>()
        {
            @Override
            public int compare(SchoolData a, SchoolData b) {
                return mComparator.compare(a, b);
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(SchoolData oldItem, SchoolData newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(SchoolData item1, SchoolData item2) {
                return item1 == item2;
            }
        });

        public void replaceAll(List<SchoolData> models) {
            mSortedList.beginBatchedUpdates();
            for (int i = mSortedList.size() - 1; i >= 0; i--) {
                final SchoolData model = mSortedList.get(i);
                if (!models.contains(model)) {
                    mSortedList.remove(model);
                }
            }
            mSortedList.addAll(models);
            mSortedList.endBatchedUpdates();
        }

        @Override
        public int getItemCount() {
            return mSortedList.size();
        }

        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SchoolData item = (SchoolData) view.getTag();
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString(ItemDetailFragment.ARG_ITEM_DBN, item.dbn);
                    arguments.putString(ItemDetailFragment.ARG_ITEM_SCHOOL_NAME, item.school_name);
                    arguments.putInt(ItemDetailFragment.ARG_ITEM_SAT_READ, item.SAT_read);
                    arguments.putInt(ItemDetailFragment.ARG_ITEM_SAT_WRITE, item.SAT_write);
                    arguments.putInt(ItemDetailFragment.ARG_ITEM_SAT_MATH, item.SAT_math);
                    arguments.putInt(ItemDetailFragment.ARG_ITEM_SAT_TAKERS, item.SAT_takers);
                    ItemDetailFragment fragment = new ItemDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_DBN, item.dbn);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_SCHOOL_NAME, item.school_name);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_SAT_READ, item.SAT_read);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_SAT_WRITE, item.SAT_write);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_SAT_MATH, item.SAT_math);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_SAT_TAKERS, item.SAT_takers);

                    context.startActivity(intent);
                }
            }
        };

        private final Comparator<SchoolData> mComparator;

        SchoolsRecyclerViewAdapter(ItemListActivity parent,
                                   boolean twoPane,
                                   Comparator<SchoolData> comparator) {
            mParentActivity = parent;
            mTwoPane = twoPane;
            mComparator = comparator;
        }

        @Override
        public SchoolViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_content, parent, false);
            return new SchoolViewHolder(view);
        }
        private void setUISATValue(TextView textview, GradientDrawable circle,int value,String def_value)
        {
            if (value<0) {
                textview.setBackground(m_stroke);
                textview.setTextColor(Color.BLACK);
            }
            else {
                textview.setBackground(circle);
                circle.setColor(getUIColorBasedOnSAT(value));
            }
            textview.setText((value>-1)?String.valueOf(value):def_value);
        }

        private void setUISATValues(SchoolViewHolder holder,SchoolData scoolData)
        {
            setUISATValue(holder.SAT_read,holder.SAT_read_circle,scoolData.SAT_read,"Read");
            setUISATValue(holder.SAT_write,holder.SAT_write_circle,scoolData.SAT_write,"Write");
            setUISATValue(holder.SAT_math,holder.SAT_math_circle,scoolData.SAT_math,"Math");

        }
/*
We are not going to use that. We are going to discard responses for recycled views by comparing the dbn when we get them back from volley
        @Override
        public void onViewRecycled(@NonNull ViewHolder holder) {
            super.onViewRecycled(holder);
            SchoolData scoolData=(SchoolData)holder.itemView.getTag();
            mParentActivity.mRequestQueue.cancelAll(TAG_VOLLEY_SAT_BY_DBN+":"+scoolData.dbn);
        }
*/
        @Override
        public void onBindViewHolder(SchoolViewHolder holder, int position, List<Object> payloads) {
//            Log.d(TAG, "payload " + payloads.toString());
            if (!payloads.isEmpty())
            {
                if (payloads.get(0) instanceof Location) {
                    bindDistance(mSortedList.get(position),holder);
                }
            } else
                {
                super.onBindViewHolder(holder, position, payloads);
            }
        }
        private void bindDistance(SchoolData schoolData,SchoolViewHolder holder)
        {
            if (null!=mMyLocation)
            {
                double distance_meters=mMyLocation.distanceTo(schoolData.location);
//                double inft = (distance_meters/0.3048);
//                long feet=Math.round(inft);
//                long inches = Math.round((inft - Math.floor(inft)) / 0.08333);
                double miles=distance_meters*0.00062137;
                String str="("+String.format("%.1f",miles)+" miles)";
                holder.distance.setText(str);
                if (miles<1)
                {
                    holder.distance.setTextColor(color_darkgreen);
                }
                else
                {
                    holder.distance.setTextColor(Color.DKGRAY);
                }
            }
        }
        @Override
        public void onBindViewHolder(final SchoolViewHolder holder, int position) {
            final SchoolData schoolData = mSortedList.get(position);
//            final SchoolData schoolData=mValues.get(position);
            holder.mView.setBackgroundColor((0==(position%2))?Color.WHITE:color_whitesmoke);
//            holder.mIdView.setText(String.valueOf(schoolData.id));
//            holder.dbn.setText(schoolData.dbn);
            holder.school_name.setText(schoolData.school_name);
            holder.address.setText(schoolData.address);
//            holder.zone.setText(scoolData.zone);
            bindDistance(schoolData,holder);
            holder.itemView.setTag(schoolData);
            holder.itemView.setOnClickListener(mOnClickListener);
            if (schoolData.SAT_read>-1) {
                setUISATValues(holder, schoolData);
            }
            else
            {
                schoolData.reset();
                setUISATValues(holder,schoolData);
                StringRequest stringRequest = new StringRequest(Request.Method.GET,
                        ItemListActivity.mFirebaseRemoteConfig.getString("URL_NYC_GOV_PREFIX_SCHOOLS_SAT_DATA")+"?dbn=" + schoolData.dbn,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONArray array = new JSONArray(response);
                                    if (array.length() > 0) {
                                        JSONObject object = array.getJSONObject(0);
                                        String dbn=object.getString("dbn");
                                        SchoolData holder_scoolData=(SchoolData)holder.itemView.getTag();
                                        if (dbn.equals(holder_scoolData.dbn)) {
                                            try {
                                                schoolData.SAT_read = Integer.parseInt(object.getString("sat_critical_reading_avg_score"));
                                            }
                                            catch (Exception ex)
                                            {
                                                schoolData.SAT_read=-1;// not a string, for example [{"dbn":"17K751","school_name":"ACADEMY FOR HEALTH CAREERS","num_of_sat_test_takers":"s","sat_critical_reading_avg_score":"s","sat_math_avg_score":"s","sat_writing_avg_score":"s"}]
                                            }
                                            try {
                                                schoolData.SAT_math = Integer.parseInt(object.getString("sat_math_avg_score"));
                                            }
                                            catch (Exception ex)
                                            {
                                                schoolData.SAT_math=-1;// not a string, for example [{"dbn":"17K751","school_name":"ACADEMY FOR HEALTH CAREERS","num_of_sat_test_takers":"s","sat_critical_reading_avg_score":"s","sat_math_avg_score":"s","sat_writing_avg_score":"s"}]
                                            }
                                            try {
                                                schoolData.SAT_write = Integer.parseInt(object.getString("sat_writing_avg_score"));
                                            }
                                            catch (Exception ex)
                                            {
                                                schoolData.SAT_write=-1;// not a string, for example [{"dbn":"17K751","school_name":"ACADEMY FOR HEALTH CAREERS","num_of_sat_test_takers":"s","sat_critical_reading_avg_score":"s","sat_math_avg_score":"s","sat_writing_avg_score":"s"}]
                                            }
                                            try {
                                                schoolData.SAT_takers= Integer.parseInt(object.getString("num_of_sat_test_takers"));
                                            }
                                            catch (Exception ex)
                                            {
                                                schoolData.SAT_takers=-1;// not a string, for example [{"dbn":"17K751","school_name":"ACADEMY FOR HEALTH CAREERS","num_of_sat_test_takers":"s","sat_critical_reading_avg_score":"s","sat_math_avg_score":"s","sat_writing_avg_score":"s"}]
                                            }
                                            setUISATValues(holder, schoolData);
                                        }
                                        else
                                        {
                                            //discard this value from volley - we got a response for a dbn that was in a view that was already recycled!
                                        }
                                    }
                                } catch (JSONException e) {
                                    Log.e(TAG,"response="+response,e);
                                    e.printStackTrace();
                                } catch(NumberFormatException e)
                                {
                                    Log.e(TAG,"response="+response,e);
                                    //Invalid integers in sat_critical_reading_avg_score/sat_math_avg_score/sat_writing_avg_score
//                                    e.printStackTrace();
                                    schoolData.reset();
                                    setUISATValues(holder,schoolData);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(mParentActivity.getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG);
                            }
                        });
//                stringRequest.setTag(TAG_VOLLEY_SAT_BY_DBN+":"+scoolData.dbn);
                stringRequest.setTag(TAG_VOLLEY_SAT_BY_DBN);
                mParentActivity.mRequestQueue.add(stringRequest);
            }
        }
/*
//we user SortedList of the values
        @Override
        public int getItemCount() {
            return mValues.size();
        }
*/
        static class SchoolViewHolder extends RecyclerView.ViewHolder {
//            final TextView mIdView;
//            final TextView dbn;
            final TextView school_name;
            final TextView address;
            final TextView distance;
//            final TextView zone;
            final TextView SAT_read;
            final TextView SAT_write;
            final TextView SAT_math;
            final GradientDrawable SAT_read_circle;
            final GradientDrawable SAT_write_circle;
            final GradientDrawable SAT_math_circle;
            final View mView;

            SchoolViewHolder(View view) {
                super(view);
                mView=view;
//                mIdView = (TextView) view.findViewById(R.id.id_text);
//                dbn = (TextView) view.findViewById(R.id.dbn);
                school_name = (TextView) view.findViewById(R.id.school_name);
                address = (TextView) view.findViewById(R.id.address);
                distance= (TextView) view.findViewById(R.id.distance);
//                zone = (TextView) view.findViewById(R.id.zone);
                SAT_read=(TextView) view.findViewById(R.id.SAT_read);
                SAT_write=(TextView) view.findViewById(R.id.SAT_write);
                SAT_math=(TextView) view.findViewById(R.id.SAT_math);
                SAT_read_circle=(GradientDrawable)SAT_read.getBackground();
                SAT_write_circle=(GradientDrawable)SAT_write.getBackground();
                SAT_math_circle=(GradientDrawable)SAT_math.getBackground();
            }
        }
    }
}
