package eu.gardi.nycschools;

import android.location.Location;

public class SchoolData {
    //TODO use setters/getters or 3rd party solution
    public final int id;
    public final String dbn;
    public final String school_name;
    public final String address;
//   public final String zone;
    public int SAT_read;
    public int SAT_write;
    public int SAT_math;
    public int SAT_takers;
    public double latitude,longitude;
    public Location location;
    public void reset()
    {
        this.SAT_read=-1;
        this.SAT_write=-1;
        this.SAT_math=-1;
        this.SAT_takers=-1;
    }
    public SchoolData(int id, String dbn, String school_name,String address,double latitude,double longitude) {
        this.id = id;
        this.dbn = dbn;
        this.school_name = school_name;
        this.address= address;
//        this.zone= zone;
        this.SAT_read=-1;
        this.SAT_write=-1;
        this.SAT_math=-1;
        this.SAT_takers=-1;
        this.latitude=latitude;
        this.longitude=longitude;
        location=new Location(dbn);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
    }

    @Override
    public String toString() {
        return "id="+id+",dbn="+dbn+",school_name="+school_name;
    }
}
